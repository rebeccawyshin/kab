#-*- coding: utf-8 -*-
import sys
# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py
sys.path.append('../../Stacking')
import Stack_regression
from flask import Flask, render_template, request, json, redirect, session
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash

import os
import sys
import urllib
import urllib2
import collections
import time

app = Flask(__name__)
app.secret_key = 'MyKeyIsSoRandom'

mysql = MySQL()
#MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '0000'
app.config['MYSQL_DATABASE_DB'] = 'kirc_kab'
app.config['MYSQL_DATABASE_HOST'] = '143.248.91.197'
mysql.init_app(app)

@app.route('/', methods=['GET', 'POST'])
def main():
	Stack_regression.init()
	if request.method == 'POST':
		#Address inputs
		sreg1 = request.form.get('sreg1')
		sreg2 = request.form.get('sreg2')
		seub = request.form.get('seub')
		ssan = request.form.get('ssan')
		sbun1 = request.form.get('sbun1')
		sbun2 = request.form.get('sbun2')
		#address = sreg1 + sreg2 + seub + ssan + sbun1 + sbun2

		#search db for pnu
		pnu = searchPNU(sreg1, sreg2, seub)
		address = pnu[:5]+ " " +pnu[5:10]+" " +ssan + " " + sbun1.zfill(4) + " " + sbun2.zfill(4)

		#search for land features and return in json format
		record = searchLandFeatures(pnu[:5], pnu[5:10], ssan, sbun1, sbun2)

		# get prediction. return -1 if there is error
		predict = -1
		try:
			predict = Stack_regression.runStackRegression(dict(record))
		except  Exception as e:
			print(e)
			predict = -1
		predDic = collections.OrderedDict()
		predDic["PRICE"] = predict

		#get Coordinates
		lat, lng = addr2Coords(sreg1, sreg2, seub, ssan, sbun1, sbun2)
		coordsDic = collections.OrderedDict()
		coordsDic["LAT"] = lat
		coordsDic["LNG"] = lng

		# translate number code in land features to Korean word
		giyuk, youngdo, gimok = featureTranslate(record)
		korRecDic = collections.OrderedDict()
		korRecDic["GIYUK"] = giyuk
		korRecDic["YOUNGDO"] = youngdo
		korRecDic["GIMOK"] = gimok

		# transform dictionary to Json format
		recordJson = json.dumps(record)
		predJson = json.dumps(predDic)
		coordsJson = json.dumps(coordsDic)
		korRecJson = json.dumps(korRecDic)

		finalRecord = {'landFeatures':recordJson, 'coordinates':coordsJson, 'predictedResult':predJson, 'koreanFeature':korRecJson}

		finalJson = json.dumps(finalRecord)
		#apply stacking

		return finalJson
	return render_template('newIndex.html')

def searchPNU(sreg1, sreg2, seub):

	conn = mysql.get_db()
	cursor = conn.cursor()

	#search for pnu code in city_region_district table
	sgg = sreg1 + " " + sreg2 + " " + seub
	query = "SELECT CODE from city_region_district where SGG='" + sgg + "'"
	#print(query)
	cursor.execute(query)
	pnuCode = cursor.fetchone()


	#print(pnuCode[0])
	return pnuCode[0]

def searchLandFeatures(sreg, seub, ssan, sbun1, sbun2):
	conn = mysql.get_db()
	cursor = conn.cursor()

	#search for land features in master table
	query = "SELECT YEAR, SREG, SEUB, SSAN, SBUN1, SBUN2, SEQNO, PYOJUNJI, AREA, GIMOK, GIMOK2, YOUNGDO, GIYUK, GIYUK2, Y1AREA, Y2AREA, DIST, FASC, FASCY, GITA, GITA2USE, GITA3, GITA4, GUBUN, OKDO, SURI, LIMYA, GOJEU, HUNG, BAN, JUB, LENGTH, TRA, HASU, HANDWORK, BIKYO, PYOJUN2, GAKUKA, GAKUKJ, GAKUKD, GAKUKS, GAKUKY, SOYUH, SOYU, BIG_PROJ, BIG_STEP, LAND_GRP, jihachul, samguri, neguri, oguri, guri, starbucks, mountain, instagram, parlot, parlot_sep, year4uni, rate_4year, iindex, popul, landtrans2, landtrans, SLOPE, ELEVATION, RECT_IDX, CONV_IDX, WL_IDX, DIST_RAIL, DIST_ROAD, DIST_SCRAP, Dist_SUB FROM ksjg where SREG= '" +sreg +"' AND SEUB = '" +seub +"' AND SSAN = '"+ssan+"' AND SBUN1='"+sbun1+"' AND SBUN2 = '"+sbun2+"'"
	#print(query)
	cursor.execute(query)
	currRecord = list(cursor.fetchone())
	print(currRecord)
	#Headers
	num_fields = len(cursor.description)
	field_names = [i[0] for i in cursor.description]
	#print(field_names)
	dic = collections.OrderedDict()
	for i in range(len(field_names)):
		dic[field_names[i]]=currRecord[i]
		#featureList.append(dic)
	return dic
	'''
	j = json.dumps(dic)
	return j
	'''

def addr2Coords(sreg1, sreg2, seub, ssan, sbun1, sbun2):
	client_id = "jqA84uQIOs1n9yUR9qDa"
	client_secret = "Rld3zzbmDH"
	encText = urllib.quote((sreg1+" " + sreg2+ " " + seub + " " + sbun1+ " - " +sbun2).encode('utf-8'))
	url = "https://openapi.naver.com/v1/map/geocode?query=" + encText # json 결과
	request = urllib2.Request(url)
	request.add_header("X-Naver-Client-Id",client_id)
	request.add_header("X-Naver-Client-Secret",client_secret)
	response = urllib2.urlopen(request)
	rescode = response.getcode()
	if(rescode==200):
		response_body = (response.read()).decode('utf-8')
		jsonResponse = json.loads(response_body)
		coords = jsonResponse['result']['items'][0]['point']
		return coords['x'], coords['y']
	else:
		print("Error Code:" + rescode)

def featureTranslate(record):

	giyukCode = record["GIYUK"]
	youngdoCode = record["YOUNGDO"]
	gimokCode = record["GIMOK"]

	conn = mysql.get_db()
	cursor = conn.cursor()
	query = "SELECT KOREAN FROM giyuk where CODE = " + str(giyukCode)
	cursor.execute(query)
	korGiyuk = cursor.fetchone()

	query = "SELECT KOREAN FROM youngdo where CODE = " + str(youngdoCode)
	cursor.execute(query)
	korYoungdo = cursor.fetchone()

	query = "SELECT KOREAN FROM gimok where CODE = " + str(gimokCode)
	cursor.execute(query)
	korGimok = cursor.fetchone()

	return korGiyuk, korYoungdo, korGimok


if __name__ == "__main__":
	app.run(debug=True)
