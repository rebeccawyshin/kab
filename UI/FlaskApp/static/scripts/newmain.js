//------------------------------------------------------------------------MAP---------------
// draw map
var mapContainer = document.getElementById('map');
var markers = [];
var mapOptions = {
	center: new daum.maps.LatLng(36.3694, 127.3640),
	level: 3
};
var map = new daum.maps.Map(mapContainer, mapOptions);

// import map controller (zoom in/out, sky view)
var mapTypeControl = new daum.maps.MapTypeControl();
map.addControl(mapTypeControl, daum.maps.ControlPosition.TOPRIGHT);
var zoomControl = new daum.maps.ZoomControl();
map.addControl(zoomControl, daum.maps.ControlPosition.RIGHT);

// move map and delete marker history and draw new marker
function panTo(lat, lng) {
    var moveLatLng = new daum.maps.LatLng(lat, lng);
    map.panTo(moveLatLng);
		setMarkers(null);
		addMarker(moveLatLng);
}

// draw marker
function addMarker(position) {
    var marker = new daum.maps.Marker({
        position: position
    });
    marker.setMap(map);
		markers.push(marker);
}

// delete or set markers
function setMarkers(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

//---------------------------------------------------------------------------DROPDOWN---------------
var sreg1; var sreg2; var seub; var ssan; var sbun1; var sbun2;

function optionItems(region) {
	k = '<Option value=' + region + '>' + region + '</Option>';
	return k;
}

//var sreg1List = ["강원도","경기도","경상남도","경상북도","광주광역시","대구광역시","대전광역시","부산광역시","서울특별시","세종특별자치시","울산광역시","인천광역시","전라남도","전라북도","제주특별자치도","충청남도","충청북도" ];
var sreg1List = ["대전광역시","부산광역시","서울특별시"];

x1 = document.getElementById('sreg1');
x1.innerHTML = '<option value="" selected disabled>&nbsp;::시도 선택::</option>' + sreg1List.map(optionItems);

var sreg1ToSreg2 = [
	["서울특별시", ["아직안돼요","종로구","중구","용산구","성동구","광진구","동대문구","중랑구","성북구","강북구","도봉구","노원구","은평구","서대문구","마포구","양천구","강서구","구로구","금천구","영등포구","동작구","관악구","서초구","강남구","송파구","강동구"]],
	["부산광역시", ["아직안돼요","중구","서구","동구","영도구","부산진구","동래구","남구","북구","해운대구","사하구","금정구","강서구","등 등"]],
	["대전광역시", ["동구","대덕구","서구","유성구","중구"]]
];

function renderNextSreg2() {
	sreg1 = document.getElementById('sreg1').value;
	for (var i=0; i<sreg1ToSreg2.length; i++){
		if (sreg1ToSreg2[i][0] == sreg1){
			x2 = document.getElementById('sreg2');
			x2.innerHTML = '<option value="" selected disabled>::시군구 선택::</option>' + sreg1ToSreg2[i][1].map(optionItems);
		}
	}
}

var sreg2ToSeub = [
	["대전광역시","동구", ["가양동","가오동","구도동","낭월동","내탑동","대동","대별동","대성동","마산동","비룡동","사성동","삼괴동","삼성동","삼정동","상소동","성남동","세천동","소제동","소호동","신상동","신안동","신촌동","신하동","신흥동","오동","용계동","용운동","용전동","원동","이사동","인동","자양동","장척동","정동","주산동","주촌동","중동","직동","천동","추동","판암동","하소동","홍도동","효동","효평동"]],
	["대전광역시","대덕구", ["갈전동","대화동","덕암동","목상동","문평동","미호동","법동","부수동","비래동","삼정동","상서동","석봉동","송촌동","신대동","신일동","신탄진동","연축동","오정동","와동","용호동","읍내동","이현동","장동","중리동","평촌동","황호동"]],
	["대전광역시","서구", ["가수원동","가장동","갈마동","관저동","괴곡동","괴정동","내동","도마동","도안동","둔산동","만년동","매노동","변동","복수동","봉곡동","산직동","삼천동","오동","용문동","용촌동","우명동","원정동","월평동","장안동","정림동","탄방동","평촌동","흑석동"]],
	["대전광역시","유성구", ["가정동","갑동","계산동","관평동","교촌동","구룡동","구성동","구암동","궁동","금고동","금탄동","노은동","대동","대정동","덕명동","덕진동","도룡동","둔곡동","문지동","반석동","방동","방현동","복용동","봉명동","봉산동","상대동","성북동","세동","송강동","송정동","수남동","신동","신봉동","신성동","안산동","어은동","외삼동","용계동","용산동","원내동","원신흥동","원촌동","자운동","장대동","장동","전민동","죽동","지족동","추목동","탑립동","하기동","학하동","화암동"]],
	["대전광역시","중구", ["구완동","금동","대사동","대흥동","목달동","목동","무수동","문창동","문화동","부사동","사정동","산성동","석교동","선화동","안영동","어남동","오류동","옥계동","용두동","유천동","은행동","정생동","중촌동","침산동","태평동","호동"]]
];

function renderNextSeub() {
	sreg2 = document.getElementById('sreg2').value;
	for (var i=0; i<sreg2ToSeub.length; i++){
		if (sreg2ToSeub[i][0] == sreg1 && sreg2ToSeub[i][1] == sreg2){
			x3 = document.getElementById('seub');
			x3.innerHTML = '<option value="" selected disabled>::읍면동 선택::</option>' + sreg2ToSeub[i][2].map(optionItems);
		}
	}
}

//-----------------------------------------------------------------------PREDICT BUTTON------------
function printValue() {
	// turn on loader
	document.getElementById('loader1').style.display = "block";
	document.getElementById('loader2').style.display = "block";
	document.getElementById('predict-button').disabled = true;
	document.getElementById('button-text').style.color = "#484848";

	// get value from dropdown
	seub = document.getElementById('seub').value;
	ssan = document.getElementById('ssan').value;
	sbun1 = document.getElementById('sbun1').value;
	sbun2 = document.getElementById('sbun2').value;

	if (sreg1 == '' || sreg2 == '' || seub == '' || ssan == '' || sbun1 == '' || sbun2 == '' ) {
		// turn off loader
		document.getElementById('loader1').style.display = "none";
		document.getElementById('loader2').style.display = "none";
		document.getElementById('predict-button').disabled = false;
		document.getElementById('button-text').style.color = "black";
		alert("주소를 모두 입력해주세요.");

	} else {
		// reset the previous results
		document.getElementById('pnu').innerHTML = "";
		document.getElementById('feature_address').innerHTML = "";
		document.getElementById('feature_giyuk').innerHTML = "";
		document.getElementById('feature_gimok').innerHTML = "";
		document.getElementById('feature_area').innerHTML = "";
		document.getElementById('feature_youngdo').innerHTML = "";
		document.getElementById('predict_unitPrice').innerHTML = "";
		document.getElementById('predict_documentArea').innerHTML = "";
		document.getElementById('predict_contractArea').innerHTML = "";
		document.getElementById('predict_price').innerHTML = "";
		document.getElementById('predict_priceRange').innerHTML = "";

		address = sreg1 + ' ' + sreg2 + ' ' + seub + ' ' + ssan + ' ' + sbun1 + ' - ' + sbun2 + '번지';
		if(ssan == '산'){
			ssan2 = 2;
		}else{
			ssan2 = 1;
		}

		// get land features and predict results from database and stacking model
		$.ajax({
			type:"POST",
			url:"/",
			data : {'sreg1':sreg1, 'sreg2':sreg2, 'seub':seub, 'ssan':ssan2, 'sbun1':sbun1, 'sbun2':sbun2},

			success:function(results){
				var retVal = JSON.parse(results);

				var landFeatures = JSON.parse(retVal.landFeatures);
				var predictedResult = JSON.parse(retVal.predictedResult);
				var coords = JSON.parse(retVal.coordinates);
				var koreanFeature = JSON.parse(retVal.koreanFeature);
				console.log(predictedResult);

				// print land features
				document.getElementById('pnu').innerHTML = landFeatures.SREG + " " + landFeatures.SEUB + " " + landFeatures.SSAN + " " + lpad(landFeatures.SBUN1,4) + " " + lpad(landFeatures.SBUN2,4);
				document.getElementById('feature_address').innerHTML = address;
				document.getElementById('feature_address').style.color = "black"
				document.getElementById('feature_giyuk').innerHTML = koreanFeature.GIYUK;
				document.getElementById('feature_gimok').innerHTML = koreanFeature.GIMOK;
				document.getElementById('feature_area').innerHTML = landFeatures.AREA + ' m<sup>2';
				document.getElementById('feature_youngdo').innerHTML = koreanFeature.YOUNGDO;
				document.getElementById('loader1').style.display = "none";

				// if prediction return -1 value,
				if (predictedResult.PRICE <= 0) {
					alert("선택하신 필지는 현재 실거래가 예측을 제공하고 있지 않습니다.");
					console.log(predictedResult.PRICE);
					document.getElementById('loader2').style.display = "none";
				} else {
					// if prediction was successful,
					var finalPrice = predictedResult.PRICE*landFeatures.AREA
					document.getElementById('predict_unitPrice').innerHTML = numberCommas(Math.round(predictedResult.PRICE)) + ' 원 / m<sup>2</sup>';
					document.getElementById('predict_documentArea').innerHTML = landFeatures.AREA + ' m<sup>2';
					document.getElementById('predict_contractArea').innerHTML = landFeatures.AREA + ' m<sup>2';
					document.getElementById('predict_price').innerHTML = numberCommas(Math.round(finalPrice)) + ' 원';
					document.getElementById('predict_priceRange').innerHTML = numberCommas(Math.round(finalPrice*0.95)) + ' 원 ~ ' + numberCommas(Math.round(finalPrice*1.05)) + ' 원';
					document.getElementById('loader2').style.display = "none";
				}

				// turn off loader
				document.getElementById('predict-button').disabled = false;
				document.getElementById('button-text').style.color = "black";

				// reset the address input box
				//document.getElementById('sreg1').value = "";
				document.getElementById('sreg2').value = "";
				document.getElementById('seub').value = "";
				document.getElementById('ssan').value = "";
				document.getElementById('sbun1').value = "";
				document.getElementById('sbun2').value = "";

				// move map and map marker - lat and lng are reversed since Naver maps returned them opposite way.
				panTo(coords.LNG, coords.LAT);
				map.setLevel(3);
			},

			error:function(error){
				console.log(error);
				// turn off loader
				document.getElementById('loader1').style.display = "none";
				document.getElementById('loader2').style.display = "none";
				document.getElementById('feature_address').innerHTML = address;
				document.getElementById('feature_address').style.color = "#787878";
				document.getElementById('predict-button').disabled = false;
				document.getElementById('button-text').style.color = "black";
				alert("입력하신 주소에 맞는 토지가 존재하지 않습니다." + "\n" + "주소가 제대로 입력되었는지 확인해주세요.");
			}
		});
	}
}

function lpad(strToPad, width, padString) {
  padString = padString || '0';
  strToPad = strToPad + '';
  return strToPad.length >= width ? strToPad : new Array(width - strToPad.length + 1).join(padString) + strToPad;
}

function numberCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
