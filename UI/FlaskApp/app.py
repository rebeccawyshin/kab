from flask import Flask, render_template, request, json, redirect, session
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash

app = Flask(__name__)
app.secret_key = 'MyKeyIsSoRandom'

mysql = MySQL()
#MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '0000'
app.config['MYSQL_DATABASE_DB'] = 'bucketlist'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


@app.route('/')
def main():
	return render_template('temp.html')

@app.route('/showSignUp')
def showSignUp():
	return render_template('signup.html')

@app.route('/signUp', methods=['POST'])
def signUp():
	#create user code, require post and request
	#read the posted values from the UI
	try:
		_name = request.form['inputName']
		_email = request.form['inputEmail']
		_password = request.form['inputPassword']

		# validate the received values
		if _name and _email and _password:
			conn = mysql.connection()
			cursor = conn.cursor()
			_hashed_password = generate_password_hash(_password)
			cursor.callproc('sp_createUser',(_name, _email, _hashed_password))
			data = cursor.fetchall()

			if len(data) is 0:
				conn.commit()
				return json.dumps({'message':'User created successfully!'})
			else:
				return json.dumps({'error':str(data[0])})
		else:
			return json.dumps({'html':'<span> Enter the required fields </span>'})
	except Exception as e:
		return json.dumps({'error':str(e)})
	finally:
		#cursor.close()
		conn.close()

@app.route('/showSignin')
def showSignin():
	if session.get('user'):
		return render_template('userHome.html')
	else:
		return render_template('signin.html')

@app.route('/validateLogin', methods=['POST'])
def validateLogin():
	try:
		_username = request.form['inputEmail']
		_password = request.form['inputPassword']


		conn = mysql.get_db()
		cursor = conn.cursor()
		cursor.callproc('sp_validateLogin', (_username,))
		data = cursor.fetchall()

		if len(data) > 0:
			if check_password_hash(str(data[0][3]),_password):
				session['user'] = data[0][0]
				return redirect('/userHome')
			else:
				return render_template('error.html', error='Wrong Email Address or Password')
		else:
			return render_template('error.html', errror = 'Wrong Email Address or Password')
	except Exception as e:
		return render_template('error.html', error=str(e))
	finally:
		#cursor.close()
		conn.close()

@app.route('/userHome')
def userHome():
	if session.get('user'):
		return render_template('userHome.html')
	else:
		return render_template('error.html', error = 'Unauthorized Access')

@app.route('/logout')
def logout():
	session.pop('user', None)
	return redirect('/')

if __name__ == "__main__":
	app.run(debug=True)
