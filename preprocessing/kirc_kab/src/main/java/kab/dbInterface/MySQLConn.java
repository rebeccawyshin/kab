package kab.dbInterface;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import kab.constants.DBConstants;
import kab.model.LandModel;

public abstract class MySQLConn {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static String DB_URL;
	private static String USERNAME = "root";
	private static String PASSWORD = "0000";
	private static Connection conn;

	public static void main(String[] args) {
		//Connection conn = MySQLConn.connect("jdbc:mysql://143.248.91.197:3306/kirc_kab?characterEncoding=utf8", "root", "0000");
	}

	/**
	 * Get MySql connection.
	 * @param hostIP: IP address to the database with port
	 * @param tableName: Table name in the DB.
	 * @return
	 */
	public static Connection connect(String hostIP, String tableName){
		DB_URL = "jdbc:mysql://" + hostIP + "/" + tableName+"?useUnicode=true&characterEncoding=utf8";
		
		try {
			if (conn == null || conn.isClosed()) {

				try {
					Class.forName(JDBC_DRIVER);
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				try {
					conn = DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
					System.out.println("\tMySQL Connection");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	
	/**
	 * Close connection to DB.
	 * @throws SQLException
	 */
	public static void closeConnection() throws SQLException{
		if (conn != null && !conn.isClosed())
			conn.close();
	}
	

	public static ResultSet getResultsFromDB(String query, Connection con, java.sql.Statement st) throws SQLException{

		st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		// NOTE! ResultSet should be closed after 
		return rs;
	}

	public static void updateDB(String query, Connection conn) throws SQLException{

		java.sql.Statement st = conn.createStatement();
		st.executeUpdate(query);
		
		st.close();
	}
	public static ArrayList<LandModel> getLandList(Connection conn){
		ArrayList<LandModel> LandList = new ArrayList<LandModel>();
		PreparedStatement pstmt = null;
		String sql = null;

		try{
			sql = "SELECT "+ DBConstants.COLUMN_NAME_YEAR +","
					+DBConstants.COLUMN_NAME_SREG +","
					+DBConstants.COLUMN_NAME_SEUB +", "
					+DBConstants.COLUMN_NAME_SSAN +", "
					+DBConstants.COLUMN_NAME_SBUN1 +", "
					+DBConstants.COLUMN_NAME_SBUN2 
					//TODO dist attr 만 업데이트 하는 경우/ none dist attr만 업데이트 하는 경우로 sql문 나누기
					+" FROM "+ DBConstants.TABLE_NAME_KSJG
					+" WHERE "
					//+ "("+DBConstants.TABLE_NAME_KSJG+"."+DBConstants.CONUMN_NAME_PO_NOT_FOUND+" IS NULL)"
					//+ " AND "
					//+" ("+DBConstants.TABLE_NAME_KSJG+"."+DBConstants.CONUMN_NAME_WL_IDX+" = 0);";
					
					//+" ("+DBConstants.TABLE_NAME_KSJG+"."+DBConstants.TABLE_NAME_INSTAGRAM+" IS NULL";
					//+" ("
					+DBConstants.TABLE_NAME_KSJG+"."+DBConstants.TABLE_NAME_JIHACHUL+" IS NULL ";
					//+ ")";
					//+" WHERE "+DBConstants.CONUMN_NAME_DIST_ROAD+" IS NULL"
					//+" OR "+DBConstants.CONUMN_NAME_RATE_4YEAR+" IS NULL";
					//+" OR "+DBConstants.CONUMN_NAME_SLOPE+" IS NULL";

			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();

			while(rs.next()){
				LandList.add(new LandModel(
						rs.getInt(DBConstants.COLUMN_NAME_YEAR),
						rs.getInt(DBConstants.COLUMN_NAME_SREG), 
						rs.getInt(DBConstants.COLUMN_NAME_SEUB), 
						rs.getInt(DBConstants.COLUMN_NAME_SSAN),
						rs.getInt(DBConstants.COLUMN_NAME_SBUN1),
						rs.getInt(DBConstants.COLUMN_NAME_SBUN2)));
			}rs.close();
			return LandList;
		}catch(SQLException se1){
			se1.printStackTrace();
			return null;
		}catch(Exception e){          
			//LOG error
			e.printStackTrace();
			System.out.println("failed to get land list from database.");
			return null;
		}finally{         
			if(pstmt != null) try{pstmt.close();}catch(SQLException sqle){sqle.printStackTrace();}      
		}
	}
	
	public static void closeResultSets(HashMap<String,ResultSet> RStables){
		Iterator<String> iterator = RStables.keySet().iterator();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			ResultSet rs = RStables.get(key);
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
