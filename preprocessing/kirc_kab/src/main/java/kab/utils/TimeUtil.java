package kab.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class TimeUtil {

	/**
	 * Calculating difference in days between two dates
	 * @param dealDate: 
	 * @param newDate
	 * @return
	 */
	public static int getDayDiff(String dealDate, String newDate){
		 Calendar cal1 = new GregorianCalendar();
		 Calendar cal2 = new GregorianCalendar();
		 
		 SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		 
		 try {
			Date date = format.parse(dealDate.toString());
			cal1.setTime(date);
			date = format.parse(String.valueOf(newDate));
			cal2.setTime(date);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 int diff = (int)((cal2.getTime().getTime() - cal1.getTime().getTime())/(1000*60*60*24));
		 return diff;
	}
	
	
	/**
	 * //Calculating month difference between two dates
	 * @param dealDate
	 * @param newDate
	 * @return
	 */
	public static int getMonthDiff(String dealDate, String newDate){
		Calendar cal1 = new GregorianCalendar();
		 Calendar cal2 = new GregorianCalendar();
		 
		 SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		 
		 try {
			Date date = format.parse(dealDate.toString());
			cal1.setTime(date);
			date = format.parse(String.valueOf(newDate));
			cal2.setTime(date);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 int diffYear = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR);
		 int diffMonth = diffYear * 12 + cal2.get(Calendar.MONTH) - cal1.get(Calendar.MONTH);
		 
		 return diffMonth;
	}
	
	/**
	 * Calculating max days in a given month
	 * @param currDate
	 * @return
	 */
	public static int getMaxDays(String currDate){
		Calendar cal1 = new GregorianCalendar();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		 
		 try {
			Date date = format.parse(currDate.toString());
			cal1.setTime(date);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 int max = cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
		 return max;
	}
	
	/**
	 * 
	 * @param dealDate : 거래날짜
	 * @param newDate: 시점 변경 날짜
	 * @return
	 */
	public static int getMaxDays(String dealDate, String newDate){
		 Calendar cal1 = new GregorianCalendar();
		 Calendar cal2 = new GregorianCalendar();
		 
		 SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		 
		 try {
			Date date = format.parse(dealDate.toString());
			cal1.setTime(date);
			date = format.parse(String.valueOf(newDate));
			cal2.setTime(date);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 int max = cal1.getActualMaximum(Calendar.DAY_OF_MONTH);
		 return max;
	}
}
