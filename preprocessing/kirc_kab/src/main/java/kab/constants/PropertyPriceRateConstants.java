package kab.constants;

import java.util.Hashtable;

public class PropertyPriceRateConstants {
	 //시점 변경 년월 YYYYMMDD 입력. ex) 2016년1월20일 --> "20160120"
	private static String adjustDate = "20160120";

	//File path
	private static final String PROPERTYRATEFILE = "PropertyPriceRate.csv";



	/*
	 * Look up table for regions in Daejeon.
	 */
	//대전의 구
	public static final Hashtable<String, String> DAEJEONREGIONS = new Hashtable<String,String>(){
		{
			put("30110","동구");
			put("30140","중구");
			put("30170","서구");
			put("30200","유성구");
			put("30230","대덕구");
		
		}
	};
	
	/*
	 * Look up table for property class.
	 */
	//용도지역
	public static final Hashtable<String, String> PROPERTYCLASS = new Hashtable<String, String>(){
		{
			put("11","주거지역");
			put("12","주거지역");
			put("13","주거지역");
			put("14","주거지역");
			put("15","주거지역");
			put("16","주거지역");
			put("21","상업지역");
			put("22","상업지역");
			put("23","상업지역");
			put("32","공업지역");
			put("33","공업지역");
			put("41","녹지지역");
			put("42","녹지지역");
			put("43","녹지지역");
			put("62","보전관리지역");
			put("63","생산관리지역");
			put("64","계획관리지역");
			put("71","농림지역");
			put("81","자연환경보전지역");
			put("71","준농림지역");
			
		}
	};
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Getters and Setters
	
	/**
	 * Gets the desired date to shift.
	 * @return
	 */
	public static String getAdjustDate() {
		return adjustDate;
	}

	/**
	 * Sets the desired date to shift.
	 * @param adjustDate
	 */
	public static void setAdjustDate(String adjustDate) {
		PropertyPriceRateConstants.adjustDate = adjustDate;
	}

	/**
	 * Gets the property rate file name
	 * @return
	 */
	public static String getPropertyratefile() {
		return PROPERTYRATEFILE;
	}
}
