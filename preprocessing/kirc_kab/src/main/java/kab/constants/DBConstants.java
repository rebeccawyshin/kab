package kab.constants;

import java.util.ArrayList;
import java.util.Arrays;

public class DBConstants {

	public static final String DATABASE_NAME = "kirc_kab";
	
	public static final String TABLE_NAME_KSJG = "ksjg_processed";
	public static final String TABLE_NAME_JOINED = "finaljoin15328";
	
	public static final String TABLE_NAME_JIGA = "jiga";
	public static final String TABLE_NAME_INSTAGRAM = "instagram";
	public static final String TABLE_NAME_DISTRICT = "city_region_district";
	public static final String TABLE_NAME_ECONVAR = "economy_var";
	public static final String TABLE_NAME_GIMOK = "gimok";
	public static final String TABLE_NAME_GIYUK = "giyuk";
	public static final String TABLE_NAME_GURI = "guri";
	/**공시지가 테이블 컬럼이름과 [치하철~스타벅스 ]테이블 이름 같음**/
	public static final String TABLE_NAME_JIHACHUL = "jihachul";
	public static final String TABLE_NAME_MOUNTAIN = "mountain";
	public static final String TABLE_NAME_NEGURI = "neguri";
	public static final String TABLE_NAME_OGURI = "oguri";
	public static final String TABLE_NAME_SAMGURI = "samguri";
	public static final String TABLE_NAME_STARBUCKS = "starbucks";
	public static final String TABLE_NAME_YOUNGDO = "youngdo";
	
	public static final String TABLE_NAME_GOLF = "golf";
	public static final String TABLE_NAME_GONGJANG = "gongjang";
	public static final String TABLE_NAME_NONGJANG = "nonggong";
	public static final String TABLE_NAME_NONGWON = "nongwon";
	
	/**names of the distance attributes that need to load table from database**/
	public static final ArrayList<String> DIST_ATTR_NAMES = new ArrayList<String>(
		Arrays.asList(
				
				DBConstants.TABLE_NAME_JIHACHUL,
				DBConstants.TABLE_NAME_SAMGURI,
				DBConstants.TABLE_NAME_NEGURI,
				DBConstants.TABLE_NAME_OGURI,
				DBConstants.TABLE_NAME_STARBUCKS,
				DBConstants.TABLE_NAME_MOUNTAIN
				/*,
				TABLE_NAME_GOLF
				TABLE_NAME_GONGJANG,
				TABLE_NAME_NONGJANG,
				TABLE_NAME_NONGWON
				*/
				)
	);
	/**needed tables for GURI**/
	public static final ArrayList<String> TABLES_FOR_GURI = new ArrayList<String>(
		Arrays.asList(
				DBConstants.TABLE_NAME_SAMGURI,
				DBConstants.TABLE_NAME_NEGURI,
				DBConstants.TABLE_NAME_OGURI
				)
	);
	
	public static final String COLUMN_NAME_YEAR = "YEAR";
	public static final String COLUMN_NAME_SREG = "SREG";
	public static final String COLUMN_NAME_SEUB = "SEUB";
	public static final String COLUMN_NAME_SSAN = "SSAN";
	public static final String COLUMN_NAME_SBUN1 = "SBUN1";
	public static final String COLUMN_NAME_SBUN2 = "SBUN2";
	
	public static final String COLUMN_NAME_LAT = "lat";
	public static final String COLUMN_NAME_LNG = "lng";

	public static final String COLUMN_NAME_PARLOT = "parlot";
	public static final String COLUMN_NAME_PARLOT_SEP = "parlot_sep";
	public static final String COLUMN_NAME_4YEARUNI = "year4uni";
	public static final String COLUMN_NAME_RATE_4YEAR = "rate_4year";
	public static final String COLUMN_NAME_IIDEX = "iindex";
	public static final String COLUMN_NAME_POPUL = "popul";
	public static final String COLUMN_NAME_LANDTRANS2 = "landtrans2";
	public static final String COLUMN_NAME_LANDTRANS = "landtrans";
	
	public static final String COLUMN_NAME_SLOPE = "SLOPE";
	public static final String COLUMN_NAME_ELEVATION = "ELEVATION";
	public static final String COLUMN_NAME_RECT_IDX = "RECT_IDX";
	public static final String COLUMN_NAME_CONV_IDX = "CONV_IDX";
	public static final String COLUMN_NAME_WL_IDX = "WL_IDX";
	public static final String COLUMN_NAME_DIST_RAIL = "DIST_RAIL";
	public static final String COLUMN_NAME_DIST_ROAD = "DIST_ROAD";
	public static final String COLUMN_NAME_DIST_SCRAP = "DIST_SCRAP";
	public static final String COLUMN_NAME_DIST_SUB = "DIST_SUB";
	
	public static final String COLUMN_NAME_PO_NOT_FOUND = "PO_NOT_FOUND";
	
	public static final String COLUMN_NAME_OBJ_AMT = "OBJ_AMT";
	public static final String COLUMN_NAME_ADJ_OBJ_AMT = "Adjusted_Obj_Amt";
	public static final String COLUMN_NAME_TARGET_VALUE = "Target_Value";
	
}
