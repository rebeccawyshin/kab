
# coding: utf-8

# In[1]:
import copy
import pandas
import numpy
from sklearn.cross_validation import KFold
from sklearn import metrics
from sklearn import preprocessing
from sklearn import pipeline
from sklearn import metrics
from sklearn.cross_validation import KFold
from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix
from sklearn.utils import check_array
from sklearn.externals import joblib

import time
from os import walk
import os #setConfig
import ConfigParser #setConfig
import traceback
import sys



"Constant variables"
#"nonggong","nongwon",
attribute39 = ["BIG_PROJ","BIG_STEP","LIMYA","SSAN","FASC","OKDO","SURI","golf", "gongjang","deal_ymd2","SREG","SEUB","TOT_AREA","AREA","GIMOK","YOUNGDO","GIYUK","DIST","FASCY","GITA","GUBUN","LIMYA","BAN","JUB","guri","starbucks","mountain","instagram","parlot_sep","rate_4year","popul","landtrans","SLOPE","ELEVATION","RECT_IDX","CONV_IDX","WL_IDX","DIST_RAIL","DIST_ROAD","DIST_SCRAP","Dist_SUB","Target_Value"]
nom_col_list = ["SREG","SEUB","SSAN","GIMOK","YOUNGDO","GIYUK","FASC","GITA","GUBUN","LIMYA","OKDO","SURI","BAN","JUB","BIG_PROJ","BIG_STEP"]
#lists of attributes to preprocess
attr_preproc_sqrt = ["samguri", "Dist_SUB","Target_Value"] #, "Target_Value"
attr_preproc_log1p = ["neguri", "guri", "starbucks", "DIST_ROAD"]

def init():
    config = ConfigParser.RawConfigParser()
    #print os.path.dirname(os.path.abspath(__file__))
    #print os.getcwd()
    #mingw_path = 'C:\\mingw-w64\\mingw64\\bin'
    try: 
        config.read('./Stack_regression.cfg')
        mingw_path = config.get('ev_path', 'mingw_path')

    except  Exception as e: 
        #for UI
        config.read('../../Stacking/Stack_regression.cfg')
        mingw_path = config.get('ev_path', 'mingw_path')

    os.environ['PATH'] = mingw_path + ';' + os.environ['PATH']


def newDataLoad(csvPath, random_state = None):
    # read CSV
    land_df = pandas.read_csv(csvPath, sep=',', header=0)
    # handle infinity
    land_df = land_df.replace(['Infinity'], numpy.nan)
    #data type setting
    land_df["Target_Value"] = land_df["Target_Value"].astype('float64')

    #drop unnessesary columns and do preprocessing
    land_df = preprocData(land_df)
    if "train" in csvPath:
        print ""
        print "final attributes to analyze: "
        for name in list(land_df):
            print name

    #drop rows that contains NaN
    land_df = land_df.dropna(axis=0)
    #shuffle dataframe
    land_df = shuffle(land_df, random_state=random_state)
    #sys.exit()
    #divide data into X and Y
    num_of_attributes = len(land_df.columns)

    "농장 test"
    #farm = land_df['YOUNGDO'] < 500
    #land_df = land_df[farm]
    #print land_df['YOUNGDO']
    #sys.exit()

    X = land_df.drop("Target_Value", axis=1)
    Y = land_df["Target_Value"]

    #X = land_np[:,0:num_of_attributes-1]
    #Y = numpy.asarray(land_np[:,num_of_attributes-1], dtype=numpy.float64)

    return X, Y

def preprocData(dataframe):
    headers = list(dataframe)
    
    "column name should be determined!"
    for column_name in attribute39:
        if column_name not in headers:
            print column_name, "does not exist in the input data."
            if column_name == "deal_ymd2":
                dataframe['deal_ymd2'] = dataframe['YEAR']
                print "deal_ymd2 is replaced with YEAR"
            elif column_name == "TOT_AREA":
                dataframe['TOT_AREA'] = dataframe['AREA']
                print "TOT_AREA is replaced with AREA"

    for column_name in headers:
        if column_name not in attribute39:
            try:
                dataframe = dataframe.drop(column_name, axis=1)
                #print column_name,": dropped"
            except  Exception as e: print(e)

        else: 
            #pre-processing
            if column_name in attr_preproc_sqrt:
                dataframe[column_name] = numpy.sqrt(dataframe[column_name])
                #print  column_name, ": preproc/sqrt"
            elif column_name in attr_preproc_log1p:
                dataframe[column_name] = numpy.log1p(dataframe[column_name])
                #print  column_name, ": preproc/log1p"
            #else: print column_name
            #print land_df[column_name].dtype
            #land_df[column_name] = land_df[column_name].astype('float64') 

    #print dataframe.sort(columns ='Target_Value', ascending=True)['Target_Value']
    #sys.exit()
    #sort columns by names
    dataframe = dataframe.reindex_axis(sorted(dataframe.columns), axis=1)
    return dataframe

#one hot ecode dataframe
def oneHotDF(dataframe):
    rows = len(dataframe)
    df_onehot = pandas.DataFrame()
    
    headers = list(dataframe)

    for col in headers:
        #if the column is nominal attribute, then one-hot ecode it.
        if col in nom_col_list:
            dummy_ = pandas.get_dummies(dataframe[col],prefix =col)
            df_onehot = pandas.concat([df_onehot,pandas.DataFrame(dummy_)], axis=1)
        else: 
            df_onehot = pandas.concat((df_onehot,dataframe[col]), axis=1)
    
    return df_onehot


def trainRegressor(regressor, X_train, Y_train):
    scaler = preprocessing.StandardScaler()
    pipeliner = pipeline.Pipeline([("scaler", scaler), ("regressor", regressor)])
    pipeliner.fit(X_train, Y_train)
    #regressor.fit(X_train, Y_train)

    return pipeliner


def classifyData(regressor, X_train, Y_train, div, X_test = None, Y_test= None, get_test_set = False):
    
    '''
    scaler = preprocessing.StandardScaler()
    pipeliner = pipeline.Pipeline([("scaler", scaler), ("regressor", regressor)])
    pipeliner.fit(X_train, Y_train)
    '''
    train_result = regressor.predict(X_train)
    train_mae = metrics.mean_absolute_error(Y_train, train_result)
    print "train MAE: ",train_mae

    div_ix = train_result > div
    Class1_X = X_train.loc[div_ix]
    Class1_Y = Y_train.loc[div_ix]
    Class2_X = X_train.loc[~div_ix]
    Class2_Y = Y_train.loc[~div_ix]

    train_true_label = [ y > div for y in Y_train ]
    train_pred_label = [ y > div for y in train_result ]
    print("train classifcation result: ")
    print "class2", "class1"
    print(confusion_matrix(train_true_label, train_pred_label))

    if get_test_set == True:
        Y_test_pred = regressor.predict(X_test)
        test_div_ix = Y_test_pred > div
        Class1_X_test = X_test.loc[test_div_ix]
        Class1_Y_test = Y_test.loc[test_div_ix]
        Class2_X_test = X_test.loc[~test_div_ix]
        Class2_Y_test = Y_test.loc[~test_div_ix]
        test_mae = metrics.mean_absolute_error(Y_test, Y_test_pred) 
        print "test MAE: ",test_mae

        test_true_label = [ y > div for y in Y_test ]
        test_pred_label = [ y > div for y in Y_test_pred ]
        print("test classifcation result: ")
        print "class2", "class1"
        print(confusion_matrix(test_true_label, test_pred_label))
        
        return Class1_X, Class1_Y, Class2_X, Class2_Y, Class1_X_test, Class1_Y_test, Class2_X_test, Class2_Y_test

    else:
        return Class1_X, Class1_Y, Class2_X, Class2_Y


def Stacking(X_dev, Y_dev, X_test, Y_test, clfs, metaRegressor,  n_folds = 5):

    # Ready for cross validation
    skf = KFold(n=X_dev.shape[0], n_folds=n_folds, random_state= 1)

    # Pre-allocate the data
    # numpy.zeros(m,n): create a m*n array initalize it with 0
    blend_train = numpy.zeros((X_dev.shape[0], len(clfs))) # Number of training data x Number of classifiers
    blend_test = numpy.zeros((X_dev.shape[0], len(clfs))) # Number of testing data x Number of classifiers

    cv_results = numpy.zeros((len(clfs), len(skf)))  # Number of classifiers x Number of folds
    # For each classifier, we train the number of fold times (=len(skf))
    for j, clf in enumerate(clfs):
        #mae_clf = 999
        #print ('\nTraining classifier [%s]%s' % (j, clf))
        blend_test_j = numpy.zeros((X_dev.shape[0], len(skf))) # Number of testing data x Number of folds , we will take the mean of the predictions later
        for i, (train_index, cv_index) in enumerate(skf):

            # This is the training and validation set
            train_index = train_index.tolist()
            cv_index = cv_index.tolist()
            X_train = X_dev.iloc[train_index]
            Y_train = Y_dev.iloc[train_index]
            X_cv = X_dev.iloc[cv_index]
            Y_cv = Y_dev.iloc[cv_index]
            #train model
            clf.fit(X_train, Y_train)

            # This output will be the basis for our blended classifier to train against,
            # which is also the output of our classifiers
            one_result = clf.predict(X_cv)
            blend_train[cv_index, j] = one_result
            '''
            if(score_mae < mae_clf):
                mae_clf = score_mae
                clf.fit(X_train,Y_train)
                print("copied clf %s" %(i))
            '''
        # Take the mean of the predictions of the cross validation set
        score_mae = metrics.mean_absolute_error(Y_test, clf.predict(X_test))
        print ('Clf_%d mae = %0.5f' % (j, score_mae))

    #train regressor by using whole train data set
    for c, clf in enumerate(clfs):
        clf.fit(X_dev, Y_dev)

    metaRegressor.fit(blend_train, Y_dev)
    return clfs, metaRegressor


def runStackRegression(record):
    #init
    init()
    import xgboost 

    #variables
    num_of_class = 2
    num_of_folds = 10
    n_trees = 100
    seed = 234234
    #Class division
    div =  numpy.sqrt(500000)
    test_df = pandas.DataFrame(record,index=[0])
    test_df = preprocData(test_df)

    '''  
    for y in test_df.columns:
        if test_df[y].dtype != 'float64' and test_df[y].dtype != 'int64':
            print test_df[y]
    
    if(agg[y].dtype == np.float64 or agg[y].dtype == np.int64):
          treat_numeric(agg[y])
    else:
          treat_str(agg[y])
    '''
    #load classifier'
    classifier = loadModel('classifier', '../../Stacking/load_this_model')
    #predict target value to classify
    className = None
    if classifier.predict(test_df) > div:
        className = "class1"
    else:
        className = "class2"

    clfs, metaregressor = loadStackingModel(className, '../../Stacking/load_this_model')
    #declare empty arrays to fill in
    classified_results = numpy.zeros((len(test_df), len(clfs)))
    #Stacking on class
    for c, clf in enumerate(clfs):
        #make prediction
        classified_results[:,c] = clf.predict(test_df)

    #combine result
    final_result = metaregressor.predict(classified_results)
    final_result = numpy.square(final_result)
    final_result = final_result[0]
    print final_result
    return final_result


def mean_absolute_percentage_error(y_true, y_pred):
    """
    Use of this metric is not recommended; for illustration only.
    See other regression metrics on sklearn docs:
      http://scikit-learn.org/stable/modules/classes.html#regression-metrics
    Use like any other metric
    >>> y_true = [3, -0.5, 2, 7]; y_pred = [2.5, -0.3, 2, 8]
    >>> mean_absolute_percentage_error(y_true, y_pred)
    Out[]: 24.791666666666668
    """

    y_true = check_array(y_true)
    y_pred = check_array(y_pred)

    ## Note: does not handle mix 1d representation
    #if _is_1d(y_true):
    #    y_true, y_pred = _check_1d_array(y_true, y_pred)
    return numpy.mean(numpy.abs((y_true - y_pred) / y_true)) * 100

def ToWeight(y):
    w = numpy.zeros(y.shape, dtype=float)
    ind = y != 0
    #w[ind] = 1./(y[ind]**2)
    w[ind] = 1./(y[ind]**2)
    return w

def rmspe(yhat, y):
    w = ToWeight(y)
    rmspe = numpy.sqrt(numpy.mean( w * (y - yhat)**2 ))
    return rmspe

def getResult(predictVal, actualVal):

    #MAE score
    score = metrics.mean_absolute_error(actualVal, predictVal)
    RMSE = numpy.sqrt(metrics.mean_squared_error(actualVal,predictVal))
    MAPE = mean_absolute_percentage_error(actualVal, predictVal)
    RMSPE = rmspe(predictVal, actualVal)
    temp = metrics.r2_score(actualVal,predictVal)
    final_score_cc = numpy.sqrt(temp)

    print ('Final mae = %s' % (score))
    print ('Final rmse = %s' % (RMSE))
    print ('Final MAPE = %s %%' % (MAPE))
    print ('Final RMSPE = %s %%' % (RMSPE * 100))
    print ('Final correlation coeffeicient = %s' % (final_score_cc))

def getFeatureImportance(clfs, class_name, num_of_attr, printResult = True):
    feature_importances = numpy.zeros((num_of_attr, len(clfs)))

    for j, clf in enumerate(clfs):
        try:
            feature_importances[:,j] = clf.feature_importances_
            #print fc_array[1:10,j]
        except Exception as e: 
            print str(e)

    if printResult == True:
        numpy.savetxt("./result/fi_"+class_name+".csv", feature_importances, delimiter=",")
    return feature_importances


def saveModel(path, class_name, classifier, regressors, metaRegressor):
    #save classifier
    joblib.dump(classifier, path+'/classifier.pkl')

    #save stacking level 0 regressor
    for c, model in enumerate(regressors):
        #joblib.dump(model, path+'/'+time.strftime("%Y%m%d_%H%M")+'_'+class_name+"_model"+str(c)+'.pkl')
        joblib.dump(model, path+'/'+class_name+"_model"+str(c)+'.pkl')

    #save meta regressor
    #joblib.dump(metaRegressor, path+'/'+time.strftime("%Y%m%d_%H%M")+'_'+class_name+'_metaRegressor.pkl')
    joblib.dump(metaRegressor, path+'/'+class_name+'_metaRegressor.pkl')


def loadStackingModel(className, path):
    models = []
    metaRegressor = None

    for (dirpath, dirnames, filenames) in walk(path):
        for fileName in filenames:
            if className in fileName:
                if 'model' in fileName:
                    model = joblib.load(path +'/'+ fileName) 
                    models.append(model)
                elif 'metaRegressor' in fileName:
                    metaRegressor = joblib.load(path +'/'+ fileName)

    return models, metaRegressor


def loadModel(modelName, path):
    model = None

    for (dirpath, dirnames, filenames) in walk(path):
        for fileName in filenames:
            if modelName in fileName:
                model = joblib.load(path +'/'+ fileName)

    return model