
'''
Created on 1 Apr 2015
@author: Jamie Hall
'''
import os

mingw_path = 'C:\\mingw-w64\\mingw64\\bin'

os.environ['PATH'] = mingw_path + ';' + os.environ['PATH']
import pickle
import xgboost 

import numpy 
from sklearn.cross_validation import KFold, train_test_split
from sklearn.metrics import confusion_matrix, mean_absolute_error
from sklearn.model_selection import GridSearchCV
from sklearn.datasets import load_iris, load_digits, load_boston

from Stack_regression import dataLoad, classifier, Stacking, newDataLoad, loadModel, saveModel

#Load train data and test data
X_train, Y_train = newDataLoad(csvPath="data/57/TrainFull.csv")
X_test, Y_test = newDataLoad(csvPath="data/57/FullTest_trimmed.csv")
div =  numpy.sqrt(500000)

print("Parameter optimization")
clf = xgboost.XGBRegressor()
clf.fit(X_train,Y_train)

clf = GridSearchCV(clf,
                   {
                   'max_depth': [6],
                    'n_estimators': [600],
                    'learning_rate': [0.05, 0.01] 
                    },verbose=1)
clf.fit(X_train,Y_train)
print(clf.best_score_)
print(clf.best_params_)

predictions = clf.predict(X_test)
actuals = Y_test

predictions = numpy.square(predictions)
actuals = numpy.square(actuals)

print(mean_absolute_error(actuals, predictions))

'''
print("Parameter optimization")
xgb_model = xgboost.XGBClassifier()
label = [ y > div for y in Y_train ]
xgb_model.fit(X_train, label)
clf = GridSearchCV(xgb_model,
                   {'max_depth': [2,4,6],
                    'n_estimators': [200,400,600],
                     'learning_rate': [0.1, 0.05, 0.01] }, verbose=1)

label = [ y > div for y in Y_test ]
clf.fit(X_test, label)
print clf.cv_results_['mean_test_score']
print(clf.best_score_)
print(clf.best_params_)

"regressor parameter optimization"
'''