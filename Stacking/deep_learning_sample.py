
# coding: utf-8

# In[1]:

import sklearn
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adamax
from keras.utils import np_utils
from keras.layers import Dropout
from keras.constraints import maxnorm
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import Pipeline

import Stack_regression
import pandas 
import numpy


# In[2]:


def baseline_model(first_input_dim =228):
    # create model
    model = Sequential()
    model.add(Dense(2000, input_dim= first_input_dim,init='normal', activation='relu', W_constraint=maxnorm(5),b_constraint=maxnorm(2)))
    model.add(Dropout(0.0))
    model.add(Dense(1000, init='normal', activation='relu', W_constraint=maxnorm(5))) #relu
    model.add(Dropout(0.0))
    model.add(Dense(500, init='normal', activation='relu', W_constraint=maxnorm(5))) #relu
    model.add(Dropout(0.1))
    model.add(Dense(100, init='normal', activation='relu', W_constraint=maxnorm(5))) #relu
    model.add(Dropout(0.1))
    model.add(Dense(1, init='normal'))
    # Compile model
    optimizer = Adamax(lr=0.05)
    model.compile(loss='mean_absolute_error', optimizer=optimizer)
    #model.compile(loss='mean_absolute_error', optimizer='Adamax')
    return model



# In[18]:

# fix random seed for reproducibility
seed = 234234

#Load train data and test data
X_train, Y_train = Stack_regression.newDataLoad(csvPath="data/daejeon/train.csv", random_state = seed)
X_test, Y_test = Stack_regression.newDataLoad(csvPath="data/daejeon/test.csv", random_state = seed)


#one-hot encode input data
n_xtr = len(X_train)

# concatenate input data to match number of columns after one hot encoding 
X_ = pandas.concat((X_train,X_test), axis=0)
X_ = Stack_regression.oneHotDF(X_)
# verification
#print oneHotDF(X_train[["guri", "JUB"]]).iloc[1:10]
#print X_train[["guri", "JUB"]].iloc[1:10]

# split
X_train_oh =X_.iloc[0:n_xtr]
X_test_oh = X_.iloc[n_xtr:]

# In[25]:

numpy.random.seed(seed)
# evaluate model with standardized dataset
#estimator = KerasRegressor(build_fn=baseline_model, nb_epoch=200, batch_size=10, verbose=1)
estimator = []
estimator.append(('standardize', StandardScaler()))

#model = baseline_model(first_input_dim = len(X_train.columns)) <= TODO: pass parameter to the model
estimator.append(('mlp', KerasRegressor(build_fn=baseline_model,nb_epoch=500, batch_size=100, verbose=1))) #, verbose=1
pipeline = Pipeline(estimator)


# In[26]:

pipeline.fit(X_train_oh.values, Y_train.values)
predictVal = pipeline.predict(X_test_oh)
actualVal = Y_test

#predictVal = numpy.square(predictVal)
#actualVal = numpy.square(actualVal)
Stack_regression.getResult(predictVal, actualVal)


# In[ ]:

'''
from sklearn import cross_validation
cv = cross_validation.KFold(len(xData), n_folds=2)

predictions = np.zeros(len(xData))
for traincv, testcv in cv:
    probas = pipeline.fit(xData[traincv], Y[traincv]).predict(xData[testcv])
    predictions[testcv] = probas


writefile = open("newText1.txt","w")

for i in xrange(0,len(predictions)):
	x = predictions[i]
	y = Y[i]
	newStr = "prediction:%s,actual:%s" % (x, y)
	#print(newStr)
	writefile.write(newStr)
	writefile.write("\n")

writefile.close
'''

